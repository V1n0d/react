/**
 * @jsx React.DOM
 */
var converter = new Showdown.converter();

var SingleFormContent = React.createClass({
  getInitialState: function() {
      return {
        items: [],
        objectId:''
      };
  },
  getComponentData: function() {
  	var self = this;
  	var currentUser = Parse.User.current();
    var userId = currentUser.id;
  	$.parse.get(this.props.dataTable[0],{
      where : { 'userId' : userId }
    },function(response){
      var response = response['results'];
      if(response.length>0) 
      	self.setState({items:response,objectId:response[0].objectId})
    });
  },
  componentDidMount: function() {
  	if(typeof this.props.showData !='undefined' && this.props.showData=='true'){
     this.getComponentData();
  	}
  },
  render : function(){
    return (
      <div id="content" className="col-xs-10 columns"> 
        <p > <span id="content_title"></span> <span id="display_msg"></span></p>
            <FormComponent showLabel={true} showData={this.props.showData} objectId={this.state.objectId} formComp={this.props.formComp} savedData={this.state.items} dataTable={this.props.dataTable} data={this.props.data} />
      </div>
      );
  }
})

