/** @jsx React.DOM */

var HomeComponent = React.createClass({
  render: function() {
    return (
      <div>
          <div id="row" className="row">
              <div id="sidebar_wrapper" className="col-xs-2 columns">
                  <NavComponent navConfig = {this.props.navConfig} />
              </div>
              <div id="content_wrapper">
              </div>
          </div>
      </div>
    );
  }
});

var NavComponent = React.createClass({
    render: function() {
        var navElems = [],subNavData;
        for (var subNav in this.props.navConfig){
            navElems.push(<NavHelperComponent className="heading" navName={subNav} data={subNav.data} />);
            for(var j in this.props.navConfig[subNav].items){
                if(typeof this.props.navConfig[subNav].items[j].subNav!='undefined'){
                  subNavData = this.props.navConfig[subNav].items[j].subNav.items;
                  navElems.push(<NavLiComponent navName={j} subNavData={subNavData} data={subNav.data} />);
                }else{
                  navElems.push(<NavLiComponent navName={j} data={subNav.data} />);
                }
            }
            navElems.push(<NavHelperComponent className="divider" />);
        }

        return (
                <div className="hide-for-small">
                    <div className="sidebar">
                        <nav>
                            <ul className="side-nav">
                            {navElems}
                            </ul>
                        </nav>
                    </div>
                </div>
            );
    }
});

var NavHelperComponent = React.createClass({
    render : function(){
        return(
            <li className={this.props.className}>
              {this.props.navName}
            </li>
            );
    }
});

var NavLiComponent = React.createClass({
    render : function(){
      var subNavComp = [];
      if(this.props.subNavData !='undefined'){
        subNavComp.push(<SubNavComponent subNavData={this.props.subNavData} />)
      }
        return(
            <li className={this.props.className}>
              <a title={this.props.navName} href={'#'+this.props.navName.toLowerCase()}>{this.props.navName}</a>
              {subNavComp}
            </li>
            );
    }
});

var SubNavComponent = React.createClass({
    render : function(){
        var subNavElems = [],
        hide={display:'none'};
        for(var nav in this.props.subNavData){
          subNavElems.push(<NavLiComponent navName={nav} data={this.props.subNavData[nav].data} />);
        }
        return (
            <ul style={hide}>{subNavElems}</ul>
        ); 
    }
});



React.renderComponent(
    <HomeComponent navConfig={appNav} />,
    document.body
);
//TODO: create <NavSubComponent>

