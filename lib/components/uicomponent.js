  /**
 * @jsx React.DOM
 */

 var converter = new Showdown.converter();

/*
*Form Component
* Events are triggered based on the element id and classes.
*/
var FormComponent = React.createClass({ 
  afterParse : function(elem,type,data){
    //callback for parse save and update
    console.log(type,this.props)
    if(type == "new") {
      // when new parse object created
      // It will trigger the handleSavedata function of the TbodyComponent in content components
      typeof this.props.handleSave =='function' ? this.props.handleSave(data) : '';
      $('#tfoot').find('input').val('')
      $('#tfoot').find('.userpickerValue').text('')
      $("#display_msg").text('Content Saved.')
      $("#display_msg").show().delay(4000).fadeOut();
    }else{
      //when a parse object is updated
      if(typeof this.props.showData=='undefined'){
        $(elem).find('.attr').each(function(i){
          if($(this).find('.userpickerValue').length<1 && $(this).find('.userId').length<1){
            value = $(this).find('input').val();
            $(this).find('span').text(value);
            $(this).find('input').hide();
            $(this).find('span').show();
          }
        });
        $(elem).find('.edit').hide()
        $(elem).find('.delete').show()
      }
      $("#display_msg").text('Content Updated.')
      $("#display_msg").show().delay(4000).fadeOut();
    }
  },
  removeEmptyArray : function(arr){
    var newArray = new Array();
    for(var i = 0; i<arr.length; i++)
    {
      if (arr[i])
      {
        newArray.push(arr[i]);
      }
    }
    return newArray;
  },
  saveData: function(e) {
    e.preventDefault();
    var currentUser = Parse.User.current();
    var userId = currentUser.id;
    var elem = this.getDOMNode(),
    columnObjects = {},
    objectId,self = this,
    parseClass = this.props.dataTable[0];

    //Get the column objects 
    $(elem).find('.attr').each(function(i){
      var key = $.trim($(this).removeClass('attr col').attr('class'))
      
      if(self.props.data[0][key].type == 'ARRAY'){
        value = $.trim($(this).find('span').text())
        columnObjects[key] = value.split(',')
        columnObjects[key] = self.removeEmptyArray(columnObjects[key]);
      }
      else if(self.props.data[0][key].type == 'STRING' && 
        typeof self.props.data[0][key].pointer!='undefined'){
        value =  $(this).find('span.userId').text();
        columnObjects[key] = value;
      }else if(self.props.data[0][key].type == 'STRING' && 
        self.props.data[0][key].inputComponent=='Dropdown'){
        value =  $(this).find('select').val();
        columnObjects[key] = value;
      }else{
        value = $(this).find('input').val();
        columnObjects[key] = value;
      }
      columnObjects['userId'] = userId;
      $(this).addClass('attr col');
   })
    // delete columnObjects[objectId]; //Delete object Id key
    objectId = $(elem).find('.objectId').text();
    
    if(typeof objectId !='undefined' && objectId !=''){
      //Update the current parse object
      $.parse.put(parseClass+'/'+objectId,columnObjects, function(resp){
        $.parse.get(parseClass,{
         where : { 'objectId' : resp.objectId }
       },
       function(response){

        var results = response['results'];
        self.afterParse.call(self,elem,"update",results);
      }
      );
      })
    }else{
      //Create a parse object
      $.parse.post(parseClass,columnObjects,
        function(resp){
          columnObjects['objectId'] = resp.objectId;
          self.afterParse.call(self,elem,"new",columnObjects);
        });
    }
  },
  cancelEdit: function(e) {
    e.preventDefault();
    var elem = this.getDOMNode(),
    objectId = $(elem).find('.objectId').text()
    if(objectId==''){
      $('#tfoot').hide();
      $('#tfoot').removeClass('display-block');
      $('.addRow').show();
      return;
    }
    
    var elem = this.getDOMNode(),columnObjects = {},objectId;
    $(elem).find('.attr').each(function(i){
      if($(this).find('.userpickerValue').length<1 && $(this).find('.userId').length<1){
        console.log($(this))
        value = $(this).find('.componentValue').text();
        $(this).find('input').val(value)
        $(this).find('span').text(value);
        $(this).find('input').hide();
        $(this).find('span').show();
      }
      $(elem).closest('.display-block').find('.edit').hide()
      $(elem).closest('.display-block').find('.delete').show()
    })
  },
  deleteData: function() {
    var check = confirm("Are you sure you want to delete?")
    if(!check) return;
    var elem = this.getDOMNode(),
    self = this,
    objectId = $(elem).find('.objectId').text(),
    parseClass = this.props.dataTable[0];
      //$(elem).remove();

    //Delete the current parse object
    $.parse.delete(parseClass+'/'+objectId, function(){
       //if(typeof self.props.handleSave=='function'){
       //   self.props.handleSave()
       //}else{
          $(elem).remove()
       //}
    }); 
  },
  componentDidMount : function(){
    $('.datePicker').datepicker();
  },
  render: function(){
    var formElements = [],objectId='',form='',objectIdHide={};

    if(this.props.formComp=='single'){
      form = 'form';
    }


    if(this.props.from=='existing' && typeof this.props.value!='undefined'){
      console.log(this.props,'props')

      var editHide = {display:'none'},
      deleteHide ={display:'block'};
          for (var i in this.props.data[0]){
            objectId = this.props.value['objectId']
          //console.log(this.props.data[0][i].type, this.props.data[0][i].pointer,'stat')

            if(this.props.data[0][i].type=='STRING' && typeof this.props.data[0][i].inputComponent=='undefined' 
              && typeof this.props.data[0][i].pointer=='undefined'){
                console.log(i,'key')
                //Input Component
              formElements.push(<InputComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} from='existing' value={this.props.value[i]} keyName={i} />);
              
            }else if(this.props.data[0][i].type=='STRING' 
              && this.props.data[0][i].inputComponent=='Dropdown'){
                //Dropdown Component
                formElements.push(<DropDownComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                  formComp={this.props.formComp} from='existing' value={this.props.value[i]} content={this.props.data[0][i].options} keyName={i} />);

            }else if(this.props.data[0][i].type=='DATE'){
              //DatePicker Component
              formElements.push(<DateComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} from='existing' value={this.props.value[i]} keyName={i} />);

            } else if(this.props.data[0][i].type=='BOOLEAN'){
              //Boolean Component
              formElements.push(<CheckBoxComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} from='existing' value={this.props.value[i]} content={this.props.data[0][i].options} keyName={i} />);

            } else if(this.props.data[0][i].type=='ARRAY' 
              && this.props.data[0][i].inputComponent=='UsersPicker'){
              //Users Picker Component
              formElements.push(<UsersPickerComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} from='existing' value={this.props.value[i]} keyName={i} />);

            } else if(this.props.data[0][i].type=='OBJECT'){
              //Availability Editor
              formElements.push(<AvailabilityEditor showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} from='existing' value={this.props.value[i]} keyName={i} />);

            }else if(this.props.data[0][i].type=='STRING' 
              && typeof this.props.data[0][i].pointer!='undefined'){
              //Pointer Component

              formElements.push(<PointerComponent showLabel={this.props.showLabel} dataTable={this.props.data[0][i].pointer} 
                attr={this.props.data[0][i].pointerAttribute} 
                value={this.props.value[i]} showData={this.props.showData} 
                formComp={this.props.formComp} from='existing' keyName={i} />);

            }else if(this.props.data[0][i].type.indexOf('HAS_MANY')!=-1){
              var routeType = this.props.data[0][i].type.substring(this.props.data[0][i].type.indexOf(':')+1);
              //Route Component
              formElements.push(<RouteComponent showLabel={this.props.showLabel} routeType={routeType} value={this.props.value['objectId']}
                dataTable={this.props.dataTable} savedData={this.props.savedData} showData={this.props.showData} 
                formComp={this.props.formComp} keyName={i} />);
            }
          }

    }else{
        var editHide = {display:'block'};

        for (var i in this.props.data[0]){
          //console.log(this.props.data[0][i].type=='STRING' 
          //    && typeof this.props.data[0][i].pointer!='undefined','stat')
            if(this.props.data[0][i].type=='STRING' 
              && typeof this.props.data[0][i].inputComponent=='undefined' 
              && typeof this.props.data[0][i].pointer=='undefined'){
                              //Input Component
              formElements.push(<InputComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} savedData={this.props.savedData} showData={this.props.showData} 
                formComp={this.props.formComp} keyName={i} />);

            }else if(this.props.data[0][i].type=='STRING' 
                  && this.props.data[0][i].inputComponent=='Dropdown'){
              //Dropdown Component
              formElements.push(<DropDownComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} content={this.props.data[0][i].options} savedData={this.props.savedData} keyName={i} />);

            }else if(this.props.data[0][i].type=='DATE'){
            //DatePicker Component
              formElements.push(<DateComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} savedData={this.props.savedData} keyName={i} />);

            } else if(this.props.data[0][i].type=='BOOLEAN'){
              //Boolean Component
              formElements.push(<CheckBoxComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} showData={this.props.showData} 
                formComp={this.props.formComp} content={this.props.data[0][i].options} savedData={this.props.savedData} keyName={i} />);

            } else if(this.props.data[0][i].type=='ARRAY' 
              && this.props.data[0][i].inputComponent=='UsersPicker'){
              //Users Picker Component
              formElements.push(<UsersPickerComponent showLabel={this.props.showLabel} dataTable={this.props.dataTable} savedData={this.props.savedData} showData={this.props.showData} 
                formComp={this.props.formComp} data={this.props.data[0]} keyName={i} />);

            } else if(this.props.data[0][i].type=='OBJECT'){
              //Availability Editor
              formElements.push(<AvailabilityEditor showLabel={this.props.showLabel} dataTable={this.props.dataTable} savedData={this.props.savedData} showData={this.props.showData} 
                formComp={this.props.formComp} keyName={i} />);

            } else if(this.props.data[0][i].type=='STRING' 
              && typeof this.props.data[0][i].pointer!='undefined'){
              //Pointer Component
                formElements.push(<PointerComponent showLabel={this.props.showLabel} dataTable={this.props.data[0][i].pointer}
                  attr={this.props.data[0][i].pointerAttribute} 
                  savedData={this.props.savedData} showData={this.props.showData} 
                  formComp={this.props.formComp} keyName={i} />);
            }
        }
      }

      if(this.props.formComp=='single'){
        var editComp = <EditComponent formComp={this.props.formComp} save={this.saveData} from={this.props.from} cancel={this.cancelEdit}  />
      }else{
        var editComp = <EditComponent from={this.props.from} save={this.saveData} cancel={this.cancelEdit}  />
        var delComp = <DeleteComponent from={this.props.from} delete={this.deleteData}/>
      }

      if(typeof this.props.showData!='undefined' && this.props.showData=='true' ){
        objectId = this.props.objectId
        objectIdHide = {display:'none'}
      }

      if(this.props.showLabel){
        objectIdHide = {display:'none'}
      }else{
        objectIdHide = {display:'inline-block'}
      }

      return (<div className={form+" display-block"}>
        <div style={objectIdHide} className='objectId first col'>
        {objectId}
        </div>
        <div className='main-fields' >
        {formElements}
        </div>
        <div className="action last col">
        {editComp}
        {delComp}
        </div>
        </div>);
    }
  });


  var EditComponent = React.createClass({

    render : function(){
      var displaySave = {},displayCancel={}
      if(this.props.formComp=='single'){

        if(this.props.from=='existing'){
          displaySave = {display:'none'};
        }else{
          displaySave = {display:'block'}; 
        }
        displayCancel = {display:'none'}

      }else{
        if(this.props.from=='existing'){
          displaySave = {display:'none'};
          displayCancel = {display:'none'}
        }else{
          displaySave = {display:'block'}; 
          displayCancel = {display:'block'}
        }
      }
      return (<div>
        <a className="edit" style={displaySave} onClick={this.props.save} href="#">Save</a>
        <a className="edit" style={displayCancel} onClick={this.props.cancel}>Cancel</a>
        </div>)
    }
  })


  var DeleteComponent = React.createClass({
    render : function(){
      console.log(this.props,'del')
      if(this.props.from=='existing'){
        var displayDelete = {display:'block'};

      }else{
       var displayDelete = {display:'none'}; 
     }
     return (<div>
      <a className="alert delete" style={displayDelete} onClick={this.props.delete}>Delete</a>
      </div>)
   }
 })

  var InputComponent = React.createClass({
    doubleClick : function(e){
      e.preventDefault();
      if (window.getSelection)
        window.getSelection().removeAllRanges();
      else if (document.selection)
        document.selection.empty();
      var elem = this.getDOMNode();
      $(elem).find('span').hide()
      $(elem).find('input').show()
      $(elem).closest('.display-block').find('.delete').hide()
      $(elem).closest('.display-block').find('.edit').show()
    },
    componentDidUpdate: function(){
      if(typeof this.props.showData !='undefined' && this.props.showData=='true' 
        && typeof this.props.savedData[0]!='undefined'){
        $('.form .main-fields').find('input').each(function(){
          var val = $(this).closest('.attr').find('.componentValue').text()
          $(this).val(val)
        })
      }

      if(typeof this.props.value!='undefined'){
        $(this.getDOMNode()).find('input').val(this.props.value)
        //$(this.getDOMNode()).find('label').text(this.props.value)
      }

    },
    render : function(){
      var value= '',label='', styleHide={},spanHide={},col='';
      if(typeof this.props.showData !='undefined' && this.props.showData=='true' 
          && typeof this.props.savedData[0]!='undefined'){
          value = this.props.savedData[0][this.props.keyName]
          spanHide = {
            display : 'none'
          }
      }else{
        if(this.props.value){
          value = this.props.value;
        }
      }
      if(this.props.from=='existing'){
        styleHide = {
          display : 'none'
        }
      }else{
        styleHide = {
          display : 'block'
        }
      }

      if(this.props.keyName!='undefined'){
        label=this.props.keyName
      }

      if(this.props.showLabel){
        labelHide = {
          display : 'inline'
        }
        col =''
      }else{
        labelHide = {
          display : 'none'
        }
        col ='col'
      }

      return(
          <div onDoubleClick={this.doubleClick} className={this.props.keyName + ' attr '+col}>
          <label style={labelHide}>{label}</label>
          <span className="componentValue" style={spanHide}>{value}</span>
          <input style={styleHide} className="inputBox" type='text' defaultValue={value} width='100'/>
          </div>
        );
    }
});



  var DropDownComponent = React.createClass({
  componentDidUpdate : function(){
      if(typeof this.props.showData !='undefined' && this.props.showData=='true' 
        && typeof this.props.savedData[0]!='undefined'){
        $('.form .main-fields').find('select').each(function(){
          var val = $(this).closest('.attr').find('.componentValue').text()
          $(this).val(val)
        })
    }
  },
  render : function(){
    var options = [],label='',col=''
    value= '', styleHide={},spanHide={};
    if(typeof this.props.showData !='undefined' && this.props.showData=='true' 
      && typeof this.props.savedData[0]!='undefined'){
      value = this.props.savedData[0][this.props.keyName]
    spanHide = {
      display : 'none'
    }
  }else{
    if(this.props.value){
      value = this.props.value;
    }
  }

  if(this.props.from=='existing'){
    styleHide = {
      display : 'none'
    }
  }
  for (var i in this.props.content){
    options.push(<DropDownOptions val={this.props.content[i]} />);
  }

  if(this.props.formComp=='single'){
    label=this.props.keyName
  }
  if(this.props.showLabel){
    labelHide = {
      display : 'inline'
    }
    col =''
  }else{
    labelHide = {
      display : 'none'
    }
    col ='col'
  }
  return(
    <div className={this.props.keyName + ' attr '+col}>
    <label>{label}</label>
    <span className="componentValue" style={spanHide}>{value}</span>
    <select className="selectBox" style={styleHide}>{options}</select>
    </div>
    );
}
});

  var DropDownOptions = React.createClass({
    render : function(){
      return(
        <option>{this.props.val}</option>
        );
    }
  });


  var CheckBoxComponent = React.createClass({
    render : function(){
      var checkboxGroup=[],label='';
      for (var i in this.props.content){
        checkboxGroup.push(<CheckBoxOptions key={i} val={this.props.content[i]} />);
      }
      if(this.props.formComp=='single'){
        label=this.props.keyName
      }
      return(
        <div className={this.props.keyName + ' attr col'}>
        <label>{label}</label>{checkboxGroup}
        <span ></span>
        </div>
        );
    }
  });

  var CheckBoxOptions = React.createClass({
    render : function(){
      var checked='';
      if(this.props.val[Object.keys(this.props.val)[0]]){
        checked = 'checked';
      }else{
        checked = "";
      }
      return(
        <div>
        <label>{Object.keys(this.props.val)[0]}</label><input type="checkbox" checked={checked} />
        </div>
        );
    }
  });

  var PointerComponent= React.createClass({
  getInitialState: function() {
      return {data: []};
  },
  componentDidMount: function(){
    //console.log(this.props)
    if(typeof this.props.value!='undefined'){
      var currentUser = Parse.User.current();
      var userId = currentUser.id;
      var id = this.props.pointer=='currentuser' ? currentUser.id : this.props.value;
      var self = this;
      var dataTable = this.props.dataTable=='users' || this.props.dataTable=='currentuser'? 
      'users' : this.props.dataTable.toTitleCase();
      $.parse.get(dataTable,{
       where : { 'objectId' : id },
     },function(response){
      console.log(dataTable)
      self.setState({data:response['results'][0]})
    }); 
    }
  },
  render : function(){
    var value='',id='',col=''
    var styleHide = {
      display : 'none'
    }
    
    if(typeof this.state.data[this.props.attr]!='undefined'){
      value = this.state.data[this.props.attr];
      id = this.state.data.objectId;
    }


    if(this.props.showLabel){
      labelHide = {
        display : 'inline'
      }
      col =''
    }else{
      labelHide = {
        display : 'none'
      }
      col ='col'
    }
    
    return(
      <div className={this.props.keyName + ' attr '+col}>
      <span className="userId" style={styleHide}>{id}</span><span>{value}</span>
      </div>
      );
  }
});


  var DateComponent= React.createClass({
  componentDidUpdate : function(){
    if(typeof this.props.showData !='undefined' && this.props.showData=='true' 
        && typeof this.props.savedData[0]!='undefined'){
        $('.form .main-fields').find('.datePicker').each(function(){
          var val = $(this).closest('.attr').find('.componentValue').text()
          $(this).datepicker( "setDate", val );
        })
    }
  },
  render : function(){
    value= '', styleHide={}, label='',spanHide={},col='';
    if(typeof this.props.showData !='undefined' && this.props.showData=='true' 
      && typeof this.props.savedData[0]!='undefined'){
      value = this.props.savedData[0][this.props.keyName]
    spanHide = {
      display : 'none'
    }
  }else{
    if(this.props.value){
      value = this.props.value;
    }
  }

  if(this.props.from=='existing'){
    styleHide = {
      display : 'none'
    }
  }
  if(this.props.formComp=='single'){
    label=this.props.keyName
  }


  if(this.props.showLabel){
    labelHide = {
      display : 'inline'
    }
    col ='col'
  }else{
    labelHide = {
      display : 'none'
    }
    col =''
  }
  return(
    <div className={this.props.keyName + ' attr '+col} >
    <label>{label}</label>
    <span className="componentValue" style={spanHide}>{value}</span>
    <input className='datePicker' style={styleHide} type='text' width='100'/>
    </div>
    );
}
});

  var UsersPickerComponent = React.createClass({
    handleCancel: function() {
      if (confirm('Are you sure you want to cancel?')) {
        $('.modal').modal('hide');
      }
    },
    autoComplete : function(data,objectId){
      function format(item) { return item.tag; };
      $("#tags").select2({
        data:{ results: data, text: 'tag' },
        formatSelection: format,
        formatResult: format
      });
      $("#tags").on('change',this.updateData)
    },
    updateData : function(e){
      $('#selected_items').append('<div id='+e.added.id+' class="tagsAdded">'+e.added.tag+'<span class="removeSelected">x</span></div>')
      $(document).on('click','.removeSelected',function(){
        console.log($(this).closest('.tagsAdded'))
        $(this).closest('.tagsAdded').remove()
      })
    },
    renderModal:function(){
      var self = this;
      var elem = $(this.getDOMNode())
      var objectId = elem.closest('.display-block').find('.objectId').text()
      var values = elem.find('.userpickerValue').text();
      values = values.split(',')
      $(elem).closest('.display-block').find('.delete').hide()
      $(elem).closest('.display-block').find('.edit').show()
      this.openModal();

      $('.modal-body').html('<input id="tags" size="50"><div id="selected_items" data-objectId='+objectId+'></div>')
      $.parse.get('users',{
       where : { 'objectId' : {'$in':values} },
     },
     function(response){
      var results = response['results'];
      for (var i in results){
        $('#selected_items').append('<div id='+results[i].objectId+' class="tagsAdded">'+results[i].name+'<span class="removeSelected">x</span></div>')
      }
    }
    );
      if(this.props.from=='existing'){
        $.parse.get('users',{
         where : { 'role' : this.props.keyName },
         order : "-createdAt"
       },
       function(response){
        var tags = []
        for (var i in response['results'] ){
          tags.push({
            id : response['results'][i].objectId,
            tag : response['results'][i].name
          })
        }
        self.autoComplete(tags,objectId,values)
      }
      );
      }else{
        $.parse.get('users',{
         where : { 'role' : this.props.keyName },
         order : "-createdAt"
       },
       function(response){
        var tags = []
        for (var i in response['results'] ){
          tags.push(
          {
            id : response['results'][i].objectId,
            tag : response['results'][i].name
          }
          )
        }
        self.autoComplete(tags,objectId,values)
      }
      );
      }

    },
    openModal:function(){
      React.renderComponent(<BootstrapModal
        cancel="Cancel"
        save = "Done"
        onCancel={this.handleCancel}
        onConfirm={this.closeModal}
        onSave = {this.handleSaveData}
        title="User Picker.">

        </BootstrapModal>
        , document.getElementById('userpicker'));
      $('.modal').modal('show');
    },
    handleSaveData : function() {
    //this.refs.modal.close();
    var self = this;
    var objectId = $('#selected_items').attr('data-objectId')
    parseClass = this.props.dataTable[0];

    var columnObjects = {}
    columnObjects[self.props.keyName] = []
    $('#selected_items .tagsAdded').each(function(){
      columnObjects[self.props.keyName].push($(this).attr('id'))
    })
    var value = columnObjects[self.props.keyName].join(',')

    if(objectId!=''){
      $('.objectId:contains("'+objectId+'")').closest('.display-block').find('.'+self.props.keyName+' .userpickerValue').text(value)
      $.parse.put(parseClass+'/'+objectId,columnObjects, function(resp){
        self.closeModal();

      })
    }else{
      $('#tfoot').find('.'+self.props.keyName+' .userpickerValue').text(value)
      this.closeModal();
    }



  },
  closeModal: function() {
    $('.modal').modal('hide');
  },
  render : function(){
    value= '',col='', label='', styleHide = {
      'display' : 'none'
    };
    if(this.props.value){
      value = this.props.value.join(',');

    }
    
    if(this.props.formComp=='single'){
      label=this.props.keyName
    }


    if(this.props.showLabel){
      labelHide = {
        display : 'inline'
      }
      col ='col'
    }else{
      labelHide = {
        display : 'none'
      }
      col =''
    }
    return(
      <div  className={this.props.keyName + ' attr '+col}>
      <div className="modalDisplay"></div>
      <span className="userpickerValue" style={styleHide} >{value}</span>
      <label>{label}</label><a href="#" onClick={this.renderModal} className="usersPicker">UsersPicker</a>
      </div>
      );
  }
});

  var RouteComponent = React.createClass({
    onClick: function(){
      var elem = $(this.getDOMNode())
      var objectId= elem.find('a').attr('data-objectid');
      var currentCourseId = objectId;
      localStorage.setItem('currentEnrolId',currentCourseId);
    },
    render : function(){
      var routeType = this.props.routeType.toTitleCase();
      return(
        <div className={this.props.keyName + ' attr col'}>
        <a href={'#'+this.props.routeType} data-objectid={this.props.value} onClick={this.onClick}>{routeType}</a>
        </div>
        );
    }
  });

  var AvailabilityEditor = React.createClass({
    render : function(){
      return(
        <div className={this.props.keyName + ' attr col'}>
        <span></span>
        <input  type='text' width='100'/>
        </div>
        );
    }
  });
  

