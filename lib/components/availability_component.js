/**
 * @jsx React.DOM
 */
var converter = new Showdown.converter();
var AvailabilityComponent = React.createClass({
    getInitialState: function() {
        return {slotsData: []};
    },
    handleAvailabilitySave :function(data){
        var slots = this.state.slotsData;
        slots.slotDetails = data ;
        this.setState({slotsData:slots})
    },
    componentDidMount: function(){
        var self = this;
        var currentUser = Parse.User.current();
        var userId = currentUser.id;
        $.parse.get('users',{
            where : { 'objectId' : userId },
        },function(response){
            self.setState({slotsData:response['results'][0]})
            $('#availablityTable .addSlot').on('click',function(){
                var day;
                $(this).removeClass('addSlot')
                day = $(this).attr('class');
                $(this).addClass('addSlot')
                $('#addAvailability').show()
                $('#addAvailability').attr('ref',day)
            })
        });
    },
    render: function() {
        var slotsMonday = [],
            slotsTuesday = [],
            slotsWednesday = [],
            slotsThursday = [],
            slotsFriday = [],
            slotsSaturday = [],
            slotsSunday = [];
        self = this;
        if(typeof this.state.slotsData.slotDetails=='undefined') {
            return (<div></div>)
        }else{
            this.state.slotsData.slotDetails.forEach(function(val,i) {
                switch(val.day){
                    case 'monday':
                        slotsMonday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                    case 'tuesday':
                        slotsTuesday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                    case 'wednesday':
                        slotsWednesday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                    case 'thursday':
                        slotsThursday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                    case 'friday':
                        slotsFriday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                    case 'saturday':
                        slotsSaturday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                    case 'sunday':
                        slotsSunday.push(<AvailabilitySlotsComponent key={i} handleAvailabilitySave={self.handleAvailabilitySave} objectId={self.state.slotsData.objectId} slotDetails={self.state.slotsData.slotDetails} data={val.slots} day ={val.day} />)
                        break;
                }
            })
        }


        return (

            <div id="availablityContainer">


                <ul id="availablityTable" className="col-xs-8 columns availablity-table">
                    <li>
                        <div className="day"> Monday </div>
                        <div className="all-slots">
            {slotsMonday}
                        </div>
                        <a className= "monday addSlot" ref={this.props.day} >Add</a>
                    </li>
                    <li>
                        <div className="day"> Tuesday </div>
                        <div className="all-slots">
            {slotsTuesday}
                        </div>
                        <a className= "tuesday addSlot" ref={this.props.day} >Add</a>
                    </li>
                    <li>
                        <div className="day"> Wednesday </div>
                        <div className="all-slots">
            {slotsWednesday}
                        </div>
                        <a className= "wednesday addSlot" ref={this.props.day} >Add</a>
                    </li>
                    <li>
                        <div className="day"> Thursday </div>
                        <div className="all-slots">
            {slotsThursday}
                        </div>
                        <a className= "thursday addSlot" ref={this.props.day} >Add</a>
                    </li>
                    <li>
                        <div className="day"> Friday </div>
                        <div className="all-slots">
            {slotsFriday}
                        </div>
                        <a className= "friday addSlot" ref={this.props.day} >Add</a>
                    </li>
                    <li>
                        <div className="day"> Saturday </div>
                        <div className="all-slots">
            {slotsSaturday}
                        </div>
                        <a className= "saturday addSlot" ref={this.props.day} >Add</a>
                    </li>
                    <li>
                        <div className="day"> Sunday </div>
                        <div className="all-slots">
            {slotsSunday}
                        </div>
                        <a className= "sunday addSlot" ref={this.props.day} >Add</a>
                    </li>
                </ul>
                <AddAvailabilityComponent handleAvailabilitySave={this.handleAvailabilitySave} objectId={this.state.slotsData.objectId} slotDetails={this.state.slotsData.slotDetails} />

                <p/>
                <b>No timezone specified.</b>
                <br/>
                <a href="./thirdparty/tz-picker/gmaps_example.html">Make sure you select your timezone!</a>

            </div>
            );
    }
});

var AddAvailabilityComponent = React.createClass({

    setAvailability : function(){
        var fromH = $(this.refs.avlFromH.getDOMNode()).val(),
            fromM = $(this.refs.avlFromM.getDOMNode()).val(),
            fromT = $(this.refs.avlFromT.getDOMNode()).val(),
            toH = $(this.refs.avlToH.getDOMNode()).val(),
            toM = $(this.refs.avlToM.getDOMNode()).val(),
            toT = $(this.refs.avlToT.getDOMNode()).val(),
            day = $.trim($(this.getDOMNode()).attr('ref')),
            self = this,
            columnObjects= {},
            newIndex,
            index;

        for (var i in this.props.slotDetails){
            if(this.props.slotDetails[i].day==day){

                index= this.props.slotDetails[i].slots.length;
                this.props.slotDetails[i].slots[index] = {from:{h:fromH,m:fromM,t:fromT}, to : {h:toH,m:toM,t:toT}}
                this.setState({avlSlots:this.props.slotDetails})

                var user = Parse.User.current()
                user.set('slotDetails',this.props.slotDetails)
                user.save(null,{
                    success : function(resp){
                        self.props.handleAvailabilitySave(self.props.slotDetails)
                        $('#addAvailability').hide()
                    }
                });
                return;
            }
        }

        newIndex= this.props.slotDetails.length;
        this.props.slotDetails[newIndex]={}
        this.props.slotDetails[newIndex].slots=[]
        this.props.slotDetails[newIndex].day = day;
        this.props.slotDetails[newIndex].slots.push({from:{h:fromH,m:fromM,t:fromT}, to : {h:toH,m:toM,t:toT}})
        this.setState({avlSlots:this.props.slotDetails})



        var user = Parse.User.current()
        user.set('slotDetails',this.props.slotDetails)
        user.save(null,{
            success : function(resp){
                self.props.handleAvailabilitySave(self.props.slotDetails)
                $('#addAvailability').hide()
            }
        });
        //self.props.handleAvailabilitySave(self.props.slotDetails)
        /*
         columnObjects = {
         slotDetails : this.props.slotDetails
         }
         $.parse.put('users/'+this.props.objectId,columnObjects, function(resp){
         self.props.handleAvailabilitySave(self.props.slotDetails)
         $('#addAvailability').hide()

         })*/
        return;


    },
    render : function(){
        var option = '',
            optionMin='',
            displayHide = {display:'none'};

        for (var i=1;i<=12;i++){
            var opt= '<option value="'+i+'">'+i+'</option>';
            option = option.concat(opt);
        }

        for (var i=0;i<=60;i += 5){
            var opt= '<option value="'+i+'">'+i+'</option>';
            optionMin = optionMin.concat(opt);
        }

        option = converter.makeHtml(option.toString());

        return(
            <div id="addAvailability" className="col-xs-2" style={displayHide}>
                <div>
                    <div className="title">From</div>
                    <div className="all-select-holder">
                        <div className="select-holder">
                            <select ref="avlFromH" className="" dangerouslySetInnerHTML={{__html: option}} />
                        </div>
                        <div className="colon">:</div>
                        <div className="select-holder">
                            <select ref="avlFromM" className="" dangerouslySetInnerHTML={{__html: optionMin}} />
                        </div>
                        <div className="select-holder">
                            <select ref="avlFromT"><option value="AM">AM</option><option value="PM">PM</option></select>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="title">To</div>
                    <div className="all-select-holder">
                        <div className="select-holder">
                            <select ref="avlToH" className="" dangerouslySetInnerHTML={{__html: option}} />
                        </div>
                        <div className="colon">:</div>
                        <div className="select-holder">
                            <select ref="avlToM" className="" dangerouslySetInnerHTML={{__html: optionMin}} />
                        </div>
                        <div className="select-holder">
                            <select ref="avlToT" className="left-space"><option value="AM">AM</option><option value="PM">PM</option></select>
                        </div>
                    </div>
                </div>
                <a className="addSlot" onClick={this.setAvailability}>Add Availability</a>
            </div>
            );
    }
})

var AvailabilitySlotsComponent = React.createClass({

    render : function(){
        var slots = []

        for (var i in this.props.data){
            slots.push(<AvailabilitySlotComponent handleAvailabilitySave={this.props.handleAvailabilitySave} objectId={this.props.objectId} slotDetails={this.props.slotDetails} slotDetailskey={this.props.key} slotsKey={i} day={this.props.day} slot={this.props.data[i]} />)
        }
        return (
            <div className="daySlot">
            {slots}
            </div>
            );
    }
})


var AvailabilitySlotComponent = React.createClass({
    deleteAvailability:function(){
        var slot = $(this.refs.slot.getDOMNode())
        var slotDetailskey = $.trim($(this.refs.slotDetailskey.getDOMNode()).attr('class'));
        var slotsKey = $.trim($(this.refs.slotsKey.getDOMNode()).attr('class'));
        var columnObjects= {};
        var self = this;
        delete this.props.slotDetails[slotDetailskey].slots[slotsKey];
        this.props.slotDetails[slotDetailskey].slots.splice(slotsKey,1)
        /*columnObjects = {
         slotDetails : this.props.slotDetails
         }
         $.parse.put('users/'+this.props.objectId,columnObjects, function(resp){
         self.props.handleAvailabilitySave(self.props.slotDetails)
         })*/
        var user = Parse.User.current()
        user.set('slotDetails',this.props.slotDetails)
        user.save(null,{
            success : function(resp){
                self.props.handleAvailabilitySave(self.props.slotDetails)
            }
        });

    },
    render : function(){

        return (
            <span ref="slot" className="slot">
                <span ref="slotDetailskey" className={this.props.slotDetailskey}>{this.props.slot.from.h}</span>:<span>{this.props.slot.from.m}</span> <span>{this.props.slot.from.t}</span>
            To
                <span ref="slotsKey" className={this.props.slotsKey}>{this.props.slot.to.h}</span>:<span>{this.props.slot.to.m}</span> <span>{this.props.slot.to.t}</span>
                <a onClick={this.deleteAvailability} className="destroy">x</a>
            </span>
            );
    }
})

