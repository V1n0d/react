/** @jsx React.DOM */
var Select2 = React.createClass({
    componentDidMount: function() {
        this.renderSelect2();
    },

    componentDidUpdate: function() {
        var select2 = $(this.refs.select2.getDOMNode())
        var input = select2.find('input')
        input.val(this.props.values).select2(this.props.params)
    },
    renderSelect2: function() {
        var select2 = $(this.refs.select2.getDOMNode())
        var input = select2.find('input')
        input.val(this.props.values).select2(this.props.params)
        input.on("change", this.props.change);
    },
    render: function() {
        var displayStyle = {
            width : 100
        }
        return (
            <div ref="select2">
            <input type="text" style={displayStyle}/>
            </div>
            );
    }
});







