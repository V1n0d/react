/**
 * @jsx React.DOM
 */
var converter = new Showdown.converter();

var ContentTableView = React.createClass({
  render : function(){
    return (
      <div id="content" className="col-xs-10 columns"> 
        <p> <span id="content_title"></span> <span id="display_msg"></span></p>
        <div className="col-xs-10 columns">
        <TableComponent formComp={this.props.formComp} hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} data={ this.props.data }/>
        </div>
      </div>
      );
  }
})

/*
* Table Component to initialize Table View
* This component is specific for this view.
*/
var TableComponent = React.createClass({
    addRow : function(e){
      e.preventDefault();
      $('#tfoot').show();
      $('#tfoot').addClass('display-block');
      $('.addRow').hide();
    },
    render: function() {
      var displayHide = {display:'none'}
      var displayAddRow = typeof this.props.hideAddRow!='undefined' ? {display:'none'} : {};
        return (
          <div>
            <ul className="table">
                <li id="thead" className="thead">
                        <div id="object_id" className="first col">Id</div>
                        <div className="main-fields">
                          <TblHeadComponent data={this.props.data} />
                        </div>
                </li>  
                <TbodyComponent formComp={this.props.formComp} dataTable={this.props.dataTable} data={this.props.data} />
            </ul>
            <a href="#" onClick={this.addRow} style={displayAddRow} className="button addRow tiny">Add Row</a>
          </div>
        );
    }
});

/* 
*Table Head component to generate the table head. 
*/
var TblHeadComponent = React.createClass({
  render : function(){
    var tblHeadKey = [];
    for (var key in this.props.data[0]) {
        tblHeadKey.push('<div class="col '+key+'">'+key+'</div>')
    }
    tblHeadKey = tblHeadKey+' ';
    tblHeadKey = tblHeadKey.split(',');
    tblHeadKey = tblHeadKey.join('')
    return (
      <div className="display-block" dangerouslySetInnerHTML={{__html: tblHeadKey}} />
    );
  }
})

/*
* Tablebody Component get the initial data to be loaded for all the course table
* I am looking for a way to reuse this component 
* or atleast getSourceData function from an external source file(Not sure this is a good Idea)
*/
var TbodyComponent = React.createClass({
  getInitialState: function() {
      return {
        items: []
      };
    },
  componentDidMount: function(){
    $('#loader').show();
    this.getSourceData();
  },
  getSourceData : function(){
    var self = this;
    var currentUser = Parse.User.current();
    var userId = currentUser.id;
    var condition={}
    var currentEnrolId = localStorage.getItem('currentEnrolId');
    if(this.props.dataTable[0]=='Enrollment'){
        condition = {
            where : { 'course_id' : currentEnrolId }
          }
    }
    $.parse.get(this.props.dataTable[0],condition      
    ,function(response){
      var response = response;
      self.setState({items:response})
      $('#loader').hide();
    });
  },
  handleSave :function(data){
    var items = this.state.items;
    items.results.push(data);
    this.setState({items:items})
    $('#tfoot').hide();
    $('#tfoot').removeClass('display-block');
    $('.addRow').show();
  },
  render : function(){
    var rows = [], displayHide = {display:'none'},
    displayStyleLoader = {display:'none'};
    for (var i in this.state.items){
      rows.push(<TableRow formComp={this.props.formComp} dataTable={this.props.dataTable} values={this.state.items[i]} data={this.props.data} />);
    };
    return (
          <div className="display-block">
            <span id="loader" style={displayHide}><img width="75" height="75" src="img/ajax-loader.gif"/><h3>Loading...</h3></span>
            <div id="tbody" className="tbody">
              {rows}
            </div>
            <MultipleFormComponent from='nonExisting' formComp={this.props.formComp} dataTable={this.props.dataTable} handleSave={this.handleSave}  data={this.props.data} />
          </div>
          );
  }
})


var MultipleFormComponent= React.createClass({
  render: function() {
    var displayHide = {display:'none'}
    return (
        <div id="tfoot" style={displayHide} className="tfoot">
          <FormComponent from='nonExisting' formComp={this.props.formComp} dataTable={this.props.dataTable} handleSave={this.props.handleSave}  data={this.props.data} />
        </div>
      );
  }
})

//component for displaying existing course and managing them
var TableRow = React.createClass({
      
    render: function() {
      var styleHide = {
        display : 'none'
      }
      var spanWidth = {
        width :100
      }
      var rowValue = []
      for(var i in this.props.values){
        rowValue.push(<RowValueComponent formComp={this.props.formComp} dataTable={this.props.dataTable} val={this.props.values[i]} data={this.props.data}/>)
      } 

      return (
          <div className="display-block">
             {rowValue} 
          </div>
      );
    }
});

//Table row component. Form component is generated and rendered within this component
var RowValueComponent = React.createClass({
  render: function() {
    console.log(this.props.tags,'in')
    return (
      <li>
      <FormComponent formComp={this.props.formComp} dataTable={this.props.dataTable} data={this.props.data} value={this.props.val} from='existing' />
      </li>
    );
  }
});





/*console.log([courseData])*/
//React.renderComponent(<MainContent dataTable={['Course']} data={[course]}/>, document.getElementById('container'));

