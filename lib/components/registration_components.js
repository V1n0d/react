/**
 * @jsx React.DOM
 */

var SignupForm = React.createClass({
  handleSubmit: function() {
    var nameNode = this.refs.name.getDOMNode();
    var emailNode = this.refs.email.getDOMNode();
    var passwordNode = this.refs.password.getDOMNode();
    var name = nameNode.value.trim();
    var email = emailNode.value.trim();
    var password = passwordNode.value.trim();
    var user = new Parse.User();
    user.set("name", name);
    user.set("password", password);
    user.set("role", 'student');
    user.set("username", email);
    user.set("slotDetails", []);
    user.signUp(null, {
      success: function(user) {
        location.href="/home.html"
      },
      error: function(user, error) {
        alert("Error: " + error.code + " " + error.message);
      }
    });
    return false;
  },
  render: function() {
    return (

      <form className="SignupForm" onSubmit={this.handleSubmit}>
        <h3>Sign Up</h3>

        <div className="row"> 
            <div className="small-10"> 
              <div className="row"> 
                  <div className="small-3 columns"> 
                      <label className="right inline">Name</label> 
                  </div> 
                  <div className="small-9 columns"> 
                      <input type="text" name="name" ref="name" id="right-label" placeholder="Name"/>
                  </div> 
              </div> 
            </div> 
        </div>
        <div className="row"> 
            <div className="small-10"> 
            <div className="row"> 
                <div className="small-3 columns"> 
                    <label className="right inline">Email</label> 
                </div> 
                <div className="small-9 columns"> 
                    <input type="text" name="email" ref="email" id="right-label" placeholder="Email"/>
                </div> 
            </div> 
            </div> 
        </div>
        <div className="row"> 
            <div className="small-10"> 
            <div className="row"> 
                <div className="small-3 columns"> 
                    <label ref="password" className="right inline">Password</label> 
                </div> 
                <div className="small-9 columns"> 
                    <input type="password" ref="password" name="password" id="right-label" placeholder="Password"/>
                </div> 
            </div> 
            </div> 
        </div>
        <div className="row"> 
            <div className="small-10"> 
            <div className="row"> 
                <div className="large-4 small-8 large-centered columns"> 
   
                    <input type="submit" value="Sign Up" className="postfix small button expand"/> 
                </div> 
            </div> 
            </div> 
        </div>
      </form>
    );
  }
});


var LoginForm = React.createClass({
  handleSubmit: function() {
    var emailNode = this.refs.email.getDOMNode();
    var passwordNode = this.refs.password.getDOMNode();
    var email = emailNode.value.trim();
    var password = passwordNode.value.trim();
    var user = new Parse.User();
    // console.log(user)
    Parse.User.logIn(email, password, {
        success: function(user) {
            //React.renderComponent(<NavComponent />, document.getElementById('sidebar_wrapper'));
            //alert(Parse.User.current())
            location.replace("home.html");
        },
        error: function(user, error) {
            //alert('login failed')
            alert(error.message);

        }
    });
    return false;
  },
  
  render: function() {
    return (

      <form className="LoginForm" onSubmit={this.handleSubmit}>
        <h3>Login</h3>
        <div className="row"> 
            <div className="small-10"> 
            <div className="row"> 
                <div className="small-3 columns"> 
                    <label className="right inline">Email</label> 
                </div> 
                <div className="small-9 columns"> 
                    <input type="text" name="email" ref="email" id="right-label" placeholder="Email"/>
                </div> 
            </div> 
            </div> 
        </div>
        <div className="row"> 
            <div className="small-10"> 
            <div className="row"> 
                <div className="small-3 columns"> 
                    <label ref="password" className="right inline">Password</label> 
                </div> 
                <div className="small-9 columns"> 
                    <input type="password" ref="password" name="password" id="right-label" placeholder="Password"/>
                </div> 
            </div> 
            </div> 
        </div>
        <div className="row"> 
            <div className="small-10"> 
            <div className="row"> 
                <div className="large-4 small-8 large-centered columns"> 
                    
                    <input type="submit" value="Login" className="postfix small button expand"/>
                </div> 
            </div>
            </div> 
        </div>
      </form>
    );
  }
});

//Reset Passowrd component for parse. Need to be moved to some other location
var ResetPassword = React.createClass({
  onClick: function(){
    var currentUser = Parse.User.current();
    console.log(currentUser)
    Parse.User.requestPasswordReset(currentUser._previousAttributes.username, {
      success: function(res) {
        // Password reset request was sent successfully
        console.log(res)
      },
      error: function(error) {
        // Show the error message somewhere
        alert("Error: " + error.code + " " + error.message);
      }
    });


  },
  render: function() {
    return (
      <div id="content" className="col-xs-10 columns">
          <h3>Reset Password</h3>
          <button type="button" onClick={this.onClick} className="btn btn-primary">Create Password</button>
      </div>
    );
  }
});



React.renderComponent(<LoginForm />, document.getElementById('auth_box'));
