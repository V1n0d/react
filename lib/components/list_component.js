/**
 * @jsx React.DOM
 */
var converter = new Showdown.converter();

var ContentListView = React.createClass({
  render : function(){
    return (
      <div id="contents" className="col-xs-10 columns"> 
        <p> 
        	<span id="content_title"></span> 
        	<span id="display_msg"></span>
        </p>
        <ItemComponent  listAttribute={this.props.listAttribute} formComp={this.props.formComp} hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} data={ this.props.data} />
      </div>
      );
  }
});

var ItemComponent = React.createClass({
	getInitialState: function() {
      return {
        items: [],
        selectedItemId: '',
        isSelectedItemUpdated : false
      };
  },
  refreshContent: function(){
  	this.getSourceData();
		$('.list-component').show()
		$('.list-add').hide()

  },
  componentDidMount: function(){
    this.getSourceData();
  },
  getSourceData : function(){
    var self = this;
    var currentUser = Parse.User.current();
    var userId = currentUser.id;
    var condition={}
    var currentEnrolId = localStorage.getItem('currentEnrolId');
    $.parse.get(this.props.dataTable[0],condition      
    ,function(response){
      var response = response;
      selectedItemId = response.results.length > 1 ? response.results[0].objectId : '';
      self.setState({items:response,selectedItemId:selectedItemId})
      $('#loader').hide();
    });
  },getSelectedItemData: function(selectedItemId){
		this.setState({
			selectedItemId:selectedItemId,
			isSelectedItemUpdated: true
		 })
	},
	toggleItemUpdated: function(){
		this.setState({
			isSelectedItemUpdated: false
		 })
	},
	render : function(){
		var selectedItemId = typeof this.state.selectedItemId!='' ? this.state.selectedItemId : '';
		return (
				<div id="details" className="col-xs-10 columns">
					<ItemNavWrapper getSelectedItemData={this.getSelectedItemData}  listAttribute={this.props.listAttribute} selectedItemId={selectedItemId}
					 formComp={this.props.formComp} items={this.state.items.results}
					  hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} 
					  data={ this.props.data } />
	        <ItemContent refreshContent={this.refreshContent} toggleItemUpdated={ this.toggleItemUpdated } isSelectedItemUpdated={this.state.isSelectedItemUpdated} getSelectedItemData={this.getSelectedItemData} listAttribute={this.props.listAttribute} selectedItemId={selectedItemId}
					 formComp={this.props.formComp} items={this.state.items.results}
					  hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} 
					  data={ this.props.data } />
        </div>
			)
	},
});


var ItemNavWrapper = React.createClass({
  getSelectedItemContent: function(selectedItemId){
		this.props.getSelectedItemData(selectedItemId);
  },
	render : function(){
		var itemNavComponent = [];
		if(typeof this.props.items !='undefined'){
			for(var item in this.props.items){
				itemNavComponent.push(
						<ItemNavComponent getSelectedItemData={this.props.getSelectedItemData} selectedItemContent={this.getSelectedItemContent} 
						refObjectId={this.props.items[item]['objectId']} 
						listAttribute ={ this.props.items[item][this.props.listAttribute] } />
					)
			}
		}
		return (
				<div className="col-xs-2 columns">
					{itemNavComponent}
				</div>
			)
	}
});

var ItemNavComponent = React.createClass({
	onClick: function(evt){
		evt.preventDefault();
		var selectedItemId = $(this.getDOMNode()).attr('data-refid')
		this.props.getSelectedItemData(selectedItemId);
	},
	render : function(){
		var listAttribute = '',refId = '';
		if(typeof this.props.listAttribute !='undefined'){
			listAttribute = this.props.listAttribute;
			refId = this.props.refObjectId;
		}
		return (
				<div className="left-icon" data-refid={refId}>
					<a href="#"  onClick={this.onClick}>{listAttribute}</a>
				</div>
			)
	}
});

var ItemContent = React.createClass({
	getInitialState: function () {
		return {
			content : [],
			isDataLoaded : false
		}
	},
	componentDidUpdate: function(a,b){
		var self = this;
		if(this.props.isSelectedItemUpdated){
			this.props.toggleItemUpdated()
	    self.setState({isDataLoaded:false})
		}
		if(!this.state.isDataLoaded && typeof this.props.selectedItemId){
			var selectedItemId = this.props.selectedItemId;
			$.parse.get(this.props.dataTable[0],{
				where : {'objectId' : selectedItemId}
			}      
	    ,function(response){
	      var response = response;
	      self.setState({content:response.results,isDataLoaded:true})
	    });
		}
	},
	render : function(){
		return (
				<div className="col-xs-8 columns item-details">
					<div>
					<ListViewFormComponent addRow={this.addRow} refreshContent={this.props.refreshContent} formComp={this.props.formComp} selectedItemId={this.props.selectedItemId} 
        		listAttribute={this.props.listAttribute} content={this.state.content}
        		hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} 
        		data={this.props.data}/>

					<UnOrderedListComponent refreshContent={this.props.refreshContent} formComp={this.props.formComp} selectedItemId={this.props.selectedItemId} 
        		listAttribute={this.props.listAttribute} content={this.state.content}
        		hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} 
        		data={this.props.data}/>
        	</div>
				</div>
			);
	}
});

var ListViewFormComponent = React.createClass({
	addRow : function(){
		$('.list-add').show()
		$('.list-component').hide()
	},
	render: function(){

		return(
				<div>
      		<a className="add_row" onClick={this.addRow}>Add Row</a>
          <div className="list-add">
          <FormComponent from='nonExisting' formComp={this.props.formComp} dataTable={this.props.dataTable} showLabel={true} handleSave={this.props.refreshContent}  data={this.props.data} />
					</div>
				</div>
			)
	}
})

var UnOrderedListComponent = React.createClass({
	render : function(){
		return (
				 <ul className="list-component">
				 		<ListComponent refreshContent={this.props.refreshContent} formComp={this.props.formComp} selectedItemId={this.props.selectedItemId} 
        		listAttribute={this.props.listAttribute} content={this.props.content[0]}
        		hideAddRow={this.props.hideAddRow} dataTable={this.props.dataTable} 
        		data={this.props.data} />
				 </ul>
			);
	}
});

var ListComponent = React.createClass({
	render : function(){
		return (
				 <li>
      <FormComponent handleSave={this.props.refreshContent} showLabel={true} formComp={this.props.formComp} dataTable={this.props.dataTable} data={this.props.data} value={this.props.content} from='existing' />
				 </li>
			)
	}
});

