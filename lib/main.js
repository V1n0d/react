var appClasses = {};

console.log('init framework...');

function registerClasses(classConfigs) {
    //console.log(classConfigs)
    for (var c in classConfigs){
        console.log('registerClass ' + c);
        genParseInput(classConfigs.c);
    }
    _.extend(appClasses, classConfigs);
}

//Function for generating type inputs to parse for creating classes

function genParseInput(data){
    var parseData = {}
    for(var i in data){
        switch(data[i].type){
            case 'STRING' :
                val = '';
                break;
            case 'ARRAY' :
                val = [];
                break;
            case 'DATE' :
                val = new Date();
                break;
            case 'OBJECT' :
                val = {};
                break;
            case 'BOOLEAN' :
                val = true;
                break;
        }
        parseData[i] = val;
    }
    return parseData;
}

//fuction for converting a string to title case

String.prototype.toTitleCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};
