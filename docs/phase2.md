
## LMS Phase 2 Specs

[TOC]

### 1. New classes, add HAS-MANY type

The following new classes will be added, they will all be associated with a Course.

- **enrolloment**: when a student registers for a course on the site, **enrollment** object will automatically be created
- **email_template**: a course can have multiple email templates that will be automatically sent to students at specified times
- **form**: a form is a dynamic class that can be configured by admin or teacher, for example, Survey class can now be implemented as form.  A course can have multiple forms, for example surveys, quizzes, etc.
- **field**: here are defined the fields for each form.

Mostly these new classes will use the components.

#### A. New HAS-MANY field type

This will allow one-to-many relationship between classes.  For example, a course will have the following:

 - enrollments
 - email templates
 - forms

These relationships are defined like this:

```
enrollments: { type: 'HAS_MANY:enrollment' },
emails: { type: 'HAS_MANY:email_template' },
forms: { type: 'HAS_MANY:form' }
```

The special notation with the ":" lets the framework know which class is the referenced class.

When you define HAS_MANY type, as above, the class referenced will automatically get a **`course_id`** field.

HAS_MANY will also be used for the Form class, which is composed of a collection of fields.

### 2. Student Enrollment using Node.JS

Before a student is registered in the site, he must enroll in a course.  This will probably use the Stripe Checkout API:

https://stripe.com/docs/checkout

1. User goes to marketing web page.
2. The user clicks "Buy Course" button, they will be asked for an email address and payment information.
2. After the payment is made, a new user is created in Parse, and also an enrollment object will be inserted.  (The course ID will be hardcoded into the signup page)
2. Then the student will be sent the "welcome email", which has a link to login the user.
3. Upon clicking the link, the user will be asked to enter their mailing address and create a password, and then they are redirected to the logged in view.

### 3. Generic nav control

The basic idea is to generate the Nav menu from a JSON config:

```
appNav = {
    Admin: {
        roles: ['admin'],
        items: {
            Courses: { type: "MainContent", data: "course"},
            Groups: { type: "MainContent", data: "group"},
            Posts: { type: "MainContent", data: "post"},
            Pages: { type: "MainContent", data: "pages"}
        }
    },
    Teacher: {
        roles: ['teacher', 'admin'],
        items: {
            My_Courses: { type: "MainContent", data: "course"},
            New_Post: { type: "SingleFormContent", data: "post"}
        }
    },
    Student: {
        roles: ['student', 'admin'],
        items: {
            Survey: { type: "SingleFormContentShowData", data: "course"},   // (SingleFormContent, showData=true)
            Availability: { type: "AvailabilityComponent", data: "post"}
        }
    }
}
```

#### A. Create NavComponent

See nav_component.js (there's a partial implementation)

#### B. Use same NavComponent to also implement Course Admin "sub nav"

When you click on a course, it should take you to Course Admin area, this area will have it's own "sub nav", defined like this:

```
courseNav = {
    Course: {
        roles: ['teacher', 'admin'],
        items: {
            Enrollments: { type: "MainContent", data: "enrollment"},
            Emails: { type: "MainContent", data: "email"},
            Forms: { type: "MainContent", data: "form"},
        }
    }
}
```

> Written with [StackEdit](https://stackedit.io/).