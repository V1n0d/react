/** @jsx React.DOM */

//Backbone Routes

var routes = {}

routes[''] = 'courses' //default routes
routes['resetpwd'] = 'resetpwd' //Reset Password Routes

//routes creation based on the nav config

for (var subNav in appNav){
  for(var j in appNav[subNav].items){
    routes[j.toLowerCase()] = j.toLowerCase();
      // if sub navigation exists add them to routes.
      if(typeof appNav[subNav].items[j].subNav!='undefined'){
          for(var nav in appNav[subNav].items[j].subNav.items){
            routes[nav.toLowerCase()] = nav.toLowerCase();
          }
      }
  }
}


//Function for rendering components based on the route

function routeComponent(page) {
  var dataTable,data,title;
  var contentWrapper = document.getElementById('content_wrapper');
  var navElem = $('.side-nav').find('li');
  var title= page;
  var item = page.toTitleCase();

  if(page=='resetpwd'){
    React.renderComponent(<ResetPassword />, contentWrapper);
    return;
  }

  if(title.indexOf('_')!=-1){
    title = title.split('_');
    title = title.join(' ');
    item = title.toTitleCase();
    item = item.split(' ')
    item = item.join('_')
  }

  title = title.toTitleCase();
  
  navElem.removeClass('active')
  navElem.find('a[title="'+page+'"]').closest('li').addClass('active')
  $('#content_wrapper').html('');
  //console.log(appNav,'appNav')
  for(var nav in appNav){
      //console.log(appNav[nav].items[item],appNav[nav].items)
      if(typeof appNav[nav].items[item]!='undefined'){
        var component = appNav[nav].items[item]['type'];
        var data = appNav[nav].items[item]['componentData']
        data.data = [appClasses[appNav[nav].items[item].data]]
        React.renderComponent(window[component](data), contentWrapper);

      }else{
        for (var itm in appNav[nav].items){
          if(typeof appNav[nav].items[itm].subNav!='undefined' && typeof appNav[nav].items[itm].subNav.items[item]!='undefined'){
            var component = appNav[nav].items[itm].subNav.items[item]['type'];
            var data = appNav[nav].items[itm].subNav.items[item]['componentData']
            data.data = [appClasses[appNav[nav].items[itm].subNav.items[item].data]]
            React.renderComponent(window[component](data), contentWrapper);
          }
        }
      }
  }
  contentTitle = document.getElementById('content_title');
  if(contentTitle){
    contentTitle.innerHTML = title; 
  }
}

// init routes
var App = Backbone.Router.extend({
  routes: routes
});

var app = new App();

app.on('route', function(page) {
  routeComponent(page)
})

Backbone.history.start();
