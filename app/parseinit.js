$.parse.init({
    app_id : parse_config.app_id, // <-- enter your Application Id here
    rest_key : parse_config.rest_key // <--enter your REST API Key here
});


var classConfigs = {
    course: {
        name : {type: 'STRING'},
        description : {type: 'STRING'},
        teacher : {type: 'ARRAY', inputComponent: 'UsersPicker' },
//		students : {type: 'ARRAY', inputComponent: 'UsersPicker' },
        enrollments: { type: 'HAS_MANY:enrollments' },
        //emails: { type: 'HAS_MANY:emails' },
        //forms: { type: 'HAS_MANY:forms' }
    },
    post: {
        title : {type: 'STRING'},
        content : {type: 'STRING'},
        author : {type: 'STRING', pointer: 'currentuser',pointerAttribute :'name'}
    },
    survey: {
        firstName: {type: 'STRING'},
        lastName: {type: 'STRING'},
        dob: {type: 'DATE'},
        country: {type: 'STRING'},
        languages: {label: "Language", type: 'STRING', inputComponent: 'Dropdown', options: ['English','French','Spanish'] }
    },
    enrollment: {
        course_id: {type:'STRING', pointer:'course',pointerAttribute : 'name'},
        student: {type:'STRING', pointer:'users',pointerAttribute : 'name'}
    },
    email_template: {
        subject: {type: 'STRING'},
        body: {type: 'STRING'}
    },
    form: {
        label: {type: 'STRING'},
        fields: {type: 'HAS_MANY:field'}
    },
    field: {
        name: {type: 'STRING'},
        type: {type: 'STRING', inputComponent: 'Dropdown', options: ['STRING', 'DATE', 'OBJECT', 'ARRAY']},
        label: {type: 'STRING'},
        inputComponent: {type: 'STRING', inputComponent: 'Dropdown', options: ['Dropdown', 'Select2', 'Object']},
        options: {type: 'STRING'}
    }

};


registerClasses(classConfigs);


