var courseNav = {
    Course: {
        roles: ['teacher', 'admin'],
        items: {
            Enrollments: { 
                type: "ContentTableView", 
                data: "enrollment",
                componentData: {
                    formComp : 'multiple',
                    dataTable: ['Enrollment'],
                    hideAddRow : true
                }
            },
            Emails: { 
                type: "ContentTableView", 
                data: "email"
            },
            Forms: { 
                type: "ContentTableView", 
                data: "form"
            },
        }
    }
}

var appNav = {
    Admin: {
        roles: ['admin'],
        items: {
            Courses: { 
                type: "ContentTableView",
                data: "course",
                subNav : courseNav.Course,
                componentData : {
                    formComp : 'multiple', 
                    dataTable : ['Course']
                } 
            },
            Groups: { 
                type: "ContentTableView",
                data: "group",
                componentData : {
                    formComp : 'multiple', 
                    dataTable : ['Group']
                }
            },
            Posts: { 
                type: "ContentListView",
                data: "post",
                componentData : {
                    formComp : 'multiple', 
                    dataTable : ['Post'],
                    listAttribute: "title",
                    showLabel : true
                }
            },
            Pages: { 
                type: "ContentTableView",
                data: "pages",
                componentData : {
                    formComp : 'multiple', 
                    dataTable : ['Page']
                }
            }
        }
    },
    Teacher: {
        roles: ['teacher', 'admin'],
        items: {
            My_Courses: { 
                data: "course",
                type: "ContentTableView",
                componentData : {
                    formComp : 'multiple', 
                    dataTable : ['Course']
                }
            },
            New_Post: { 
                data: "post",
                type: "SingleFormContent",
                componentData : {
                    formComp : 'single', 
                    dataTable : ['Post']
                }
            }
        }
    },
    Student: {
        roles: ['student', 'admin'],
        items: {
            Survey: { 
                data: "survey",
                type: "SingleFormContent",
                componentData : {
                    formComp : 'single', 
                    showData: 'true', 
                    dataTable : ['Survey']
                }
            },   // (SingleFormContent, showData=true)
            Availability: { 
                type: "AvailabilityComponent",
                componentData : {
                    formComp : 'single' 
                }
            }
        }
    }
}

//console.log(appNav,'appNav')