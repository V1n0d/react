/**
 * @jsx React.DOM
 */

var HomeComponent = React.createClass({
  render: function() {
    return (
      <div>
          <div id="row" className="row">
              <div id="sidebar_wrapper" className="col-xs-2 columns">
                  <NavComponent />
              </div>
              <div id="content_wrapper">
              </div>
          </div>
      </div>
    );
  }
});

var CenterWrapper = React.createClass({
  render: function() {
    return (
    <div id="row" className="row">
      <SideBar />
      <div id="content_wrapper">
      </div>
    </div>
    );
  }
});
  
var CourseAdminComponent = React.createClass({
    render: function () {
        return (
            <div id="row" className="row">
                <div id="course_nav_wrapper" className="col-xs-2 columns">
                    <CourseNavComponent />
                </div>
                <div id="course_admin_wrapper">
                </div>
            </div>
            );
    }
});


// TODO: convert this to generic component that uses json definition in nav_config.js

var NavComponent = React.createClass({
  checkAuthListener : function(){
    checkAuth();
  },
  componentDidMount: function() {
     window.addEventListener('click', this.checkAuthListener);
  },
  render: function() {
      return (
      <div className="hide-for-small">
        <div className="sidebar">
          <nav>
            <ul className="side-nav">
              <AdminNavComponent />
              <li className="divider"></li>
              <StudentNavComponent />
              <li className="divider"></li>
              <TeacherNavComponent />
              <li className="divider"></li>
              <Logout />
            </ul>
          </nav>
        </div>
      </div>
    );
  }
});

var AdminNavComponent = React.createClass({
  render: function() {
    return (
      <div>
        <li className="heading">
          Admin
        </li>
        <li className="active">
          <a title="Courses"  href="#courses">Courses</a>
        </li>
        <li>
          <a  title="Groups" href="#groups">Groups</a>
        </li>
        <li>
          <a  title="Posts" href="#posts">Posts</a>
        </li>
          <li>
              <a  title="Pages" href="#pages">Pages</a>
          </li>
//          <li>
//              <a  title="Forms" href="#forms">Forms</a>
//          </li>
      </div>
    );
  }
});

var StudentNavComponent = React.createClass({
  render: function() {
    return (
      <div>
        <li className="heading">Student</li>
        <li><a href="#survey" title="Survey">Survey</a></li>
        <li><a href="#availability" title="Availability">Availability</a></li>
      </div>
    );
  }
});

var TeacherNavComponent = React.createClass({
  render: function() {
    return (
      <div>
        <li className="heading">Teacher</li>
        <li><a href="#newpost" title="New Post">New Post</a></li>
      </div>
    );
  }
});

var Logout = React.createClass({
  logoutUser:function(){
    Parse.User.logOut();
    location.replace('index.html')
  },
  render: function() {
    return (
      <div>
        <li><a href="#" onClick={this.logoutUser}>Logout</a></li>
      </div>
    );
  }
});

React.renderComponent(<HomeComponent />, document.getElementById('container'));

//Backbone Routes
var App = Backbone.Router.extend({
  
  routes: {
    "": "courses",
    "courses": "courses",
    "posts": "posts",
    "availability": "availability",
    "survey": "survey",
    "newpost": "newpost",
  }

});


function loadComponent(page) {
  var dataTable,data,title;
  var contentWrapper = document.getElementById('content_wrapper');
  var navElem = $('.side-nav').find('li')
  navElem.removeClass('active')

  switch(page){
    case 'posts' :
      title = 'Posts'
      navElem.find('a[title="'+title+'"]').closest('li').addClass('active')
      $('#content_wrapper').html('');
      React.renderComponent(<MainContent formComp='multiple' dataTable={['Post']} data={[appClasses.post]}/>, contentWrapper);
      break;
    case 'courses' :
      dataTable = 'Course'
      title = 'Courses'
      navElem.find('a[title="'+title+'"]').closest('li').addClass('active')
      $('#content_wrapper').html('');
      React.renderComponent(<MainContent formComp='multiple' dataTable={[dataTable]} data={[appClasses.course]}/>, contentWrapper);
      break;
    case 'availability' :
      title = 'Availability'
      navElem.find('a[title="'+title+'"]').closest('li').addClass('active')
      $('#content_wrapper').html('');
      React.renderComponent(<AvailabilityComponent formComp='multiple' />, contentWrapper);
      break;
    case 'newpost' :
      title = 'New Post'
      navElem.find('a[title="'+title+'"]').closest('li').addClass('active')
      $('#content_wrapper').html('');
      React.renderComponent(<SingleFormContent formComp='single' dataTable={['Post']} data={[appClasses.post]} />, contentWrapper);
      break;
    case 'survey' :
      dataTable = 'Survey'
      title = 'Survey'
      navElem.find('a[title="'+title+'"]').closest('li').addClass('active')
      $('#content_wrapper').html('');
      React.renderComponent(<SingleFormContent showData="true" formComp='single' dataTable={['Survey']} data={[appClasses.survey]} />, contentWrapper);
      break;
  }
  contentTitle = document.getElementById('content_title');
  contentTitle.innerHTML = title; 

}

var app = new App();
app.on('route', function(page) {
  loadComponent(page)
})

Backbone.history.start();


