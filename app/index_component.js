/**
 * @jsx React.DOM
 */

var IndexComponent = React.createClass({
  render: function() {
    return (
      <div>
        <HeaderComponent />
        <FormContainer />
      </div>
    );
  }
});


var HeaderComponent = React.createClass({
  renderLogin : function(){

    React.renderComponent(<LoginForm />, document.getElementById('auth_box'));

  },

  renderSignUp : function(){
    
    React.renderComponent(<SignupForm />, document.getElementById('auth_box'));

  },
  
  render: function() {
    return (
      <div className="row">
        <div id="auth_menu" className="large-9 columns">
          <ul className="inline-list right">
            <li><a href="#" onClick={this.renderLogin}>Login</a></li>
            <li><a href="#" onClick={this.renderSignUp}>| Signup</a></li>
          </ul>
        </div>
      </div>
    );
  }
});



var FormContainer = React.createClass({
  render: function() {
    return (
      <div id="row"> 
        <div className="large-8 push-3 columns">
          <div id="auth_wrapper" className="large-10 columns">
            <div id="auth_box" className="radius panel large-8 small-4 columns small-centered large-centered"></div>
          </div>
        </div>
      </div>
    );
  }
});


React.renderComponent(<IndexComponent />, document.getElementById('container'));
