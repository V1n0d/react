'use strict';
/**
 * Module dependencies.
 */
var express = require('express');
var routes = require('./buynow/routes');
var http = require('http');
var path = require('path');
var fs = require('fs');

var app = express();

// configuration
try {
  var configJSON = fs.readFileSync(__dirname + "/config.json");
  var config = JSON.parse(configJSON.toString());
} catch (e) {
  console.error("File config.json not found or is invalid: " + e.message);
  process.exit(1);
}
routes.init(config);

// all environments
app.set('port', process.env.PORT || config.port);
app.set('views', __dirname + '/buynow/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);

//setup multiple static directories
app.use(express.static(__dirname, '/buynow/public'));
app.use(express.static(__dirname + '/landing'));
app.use("/app", express.static(__dirname + '/app'));
app.use("/css", express.static(__dirname + '/css'));
app.use("/lib", express.static(__dirname + '/lib'));

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}
//console.log(routes)

// buy routes
app.get('/buy/', routes.index);
app.get('/buy/create', routes.create);
app.get('/buy/execute', routes.execute);
app.get('/buy/cancel', routes.cancel);
app.get('/welcome', routes.welcome);


http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
