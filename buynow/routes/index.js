'use strict';
var request = require('request')
var Parse = require('parse').Parse;
var paypal = require('paypal-rest-sdk');
var config = {};
var helperFunctions = {};
var mailgunAuth = {
  			username: "api",
  			password: "key-4tmjyn73a6r1tsvt-zqk88htwsor9qh0"
		}

Parse.initialize("XVmAfxO7FynFOcFoomHsnbigxONIffn348aXPKlX", "KodF54eBk0qAzKBPc5JzFAmXaOXjbjYgTbxZ5a3d");

helperFunctions = {
	sendEmail: function(emailId,subject,message){
		var options = {
		  url: 'https://api.mailgun.net/v2/sandbox11319.mailgun.org/messages',
		  method: 'POST',
		  qs: {
		  	'from': 'blee@mailgun.net',
		  	'to' : emailId,
		  	'subject' : subject,
		  	'html': message
		  },
		  auth: mailgunAuth
		};

		request(options, function (err, result){
		  if(err){ console.log(err) }
		  console.log(result.request.response.body)
		});
	},
	userLogin : function(username,password,req,res){
		Parse.User.logIn(username, password, {
		  success: function(user) {
		    // Do stuff after successful login.
		    res.redirect('http://localhost/fbReact/home1.html#resetpwd')

		  },
		  error: function(user, error) {
		    // The login failed. Check error to see why.
		  }
		});
	},
	userSignUp: function(responseData,res){
		var that = this;
		var user = new Parse.User();
		user.set("username", responseData.email_id);
		user.set("password", this.generateRandomPassword());
		user.set("role", "student");

		user.signUp(null, {
		  success: function(user) {
		    that.enrollUser(responseData,user)
		  },
		  error: function(user, error) {
		  	var emailId = user.attributes.username;
		  	var message = 'You are successfully enrolled in the course';
		  	var subject = 'Course Enrollment'
		    if(error.code == 202){
		    	that.sendEmail(emailId,subject, message);
		    	//res.redirect('http://localhost/fbReact/login.html')
		    }
		  }
		});
	},
	enrollUser : function(responseData,user){
		var Enrollment = Parse.Object.extend("Enrollment");
		var enroll = new Enrollment();
		var that = this;
		
		enroll.set("course_id", responseData.course_id);
		enroll.set("student", user.id);
		enroll.save(null, {
		  success: function(enr) {
		  	var subject = 'Welcome Email'

		  	var message = '<h3>You have successfully enrolled for the course.';
		  	message+='Please <a href="http://localhost:3000/welcome?user='+user._previousAttributes.username+'&token='+user._previousAttributes.password+'">click here</a> to login </h3>'
		    that.sendEmail(responseData.email_id,subject,message)
		    console.log('New object created with objectId: ' + enr.id);
		  },
		  error: function(enr, error) {
		    console.log('Failed to create new object, with error code: ' + error.description);
		  }
		});
	},
	generateRandomPassword : function(){
		var text = "";
    	var possible = "ABCD-_EFGH-_IJKLM-_NOPQRST-_UVWXYZ_-01234-_56789";
    	for( var i=0; i < 25; i++ )
        	text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
	},
	createPayment : function(payment,req,res){
		paypal.payment.create(payment, function (error, payment) {
			if (error) {
				res.render('error', { 'error': error });
			} else {
				req.session.paymentId = payment.id;
					for(var i=0; i < payment.links.length; i++) {
						var link = payment.links[i];

						if (link.method === 'REDIRECT') {
							var redirectUrl = link.href;
							res.redirect(redirectUrl)
						}
						
					}
					//console.log(redirectUrl)
					//helperFunctions.executePayment(redirectUrl);
				}
		});
	},
	executePayment : function(paymentId,details,req,res){
		var payment = paypal.payment.execute(paymentId, details, function (error, payment) {
			if (error) {
				//console.log(error);
				res.render('error', { 'error': error });
			} else {
				
				var responseData = {
					'course_name' : payment.transactions[0].item_list.items[0].name,
					'course_id' : payment.transactions[0].item_list.items[0].sku,
					'email_id' : payment.transactions[0].description,
					'pay_id' : payment.id,
					'pay_state' : payment.state
				}

				helperFunctions.userSignUp(responseData,res)
				//console.log(responseData)
				res.render('execute', { 'payment': responseData });
			}
		});
	}
}

// Routes

exports.index = function (req, res) {
	console.log(req)
  res.render('index');
};

exports.welcome = function(req, res) {
	var username = req.param('user')
	var password = req.param('token')
	helperFunctions.userLogin(username,password,req,res)

};

exports.create = function (req, res) {
	var method = req.param('method');

	var payment = {
		"intent": "sale",
		"payer": {
		},
		"transactions": [{
			"amount": {
				"currency": "USD",
				"total": req.param('price')
			},
			"description": req.param('description'),
			"item_list": {
            "items": [
	                {
	                    "quantity": "1",
	                    "name": req.param('course_name'),
	                    "sku": req.param('sku'),
	                    "price":req.param('price'),
	                    "currency": "USD",
	               }
            	]
        	}
		}]
	};

	if (method === 'paypal') {
		payment.payer.payment_method = 'paypal';
		payment.redirect_urls = {
			"return_url": "http://localhost:5000/buy/execute",
			"cancel_url": "http://localhost:5000/buy/cancel"
		};
	}

	helperFunctions.createPayment(payment,req,res);
	
};


exports.execute = function (req, res) {
	var paymentId = req.session.paymentId;
	var payerId = req.param('PayerID');
	var details = { "payer_id": payerId };

	helperFunctions.executePayment(paymentId,details,req,res);
	
};

exports.cancel = function (req, res) {
  res.render('cancel');
};

// Configuration


exports.init = function (c) {
	config = c;
	paypal.configure(c.api);
};