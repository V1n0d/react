We need to create some very simple tests that can test individual components.  For example

select2_test.js
uicomponents_test.js

These tests should NOT depend on parse (i.e. they should unit tests just of the components)

I'm still not sure what test framework is best for these tests.