/**
 * @jsx React.DOM
 */
var AdminPanel = React.createClass({
    render: function () {

        var crudTable = null;
        crudTable = (<CrudTable/>);
        return (
            <div className="row">
                <div className="col-md-4">
                    <ul className="nav nav-stacked">
                        <li className="active">
                            <a href="#">Users</a>
                        </li>
                        <li>
                            <a href="#" onClick={this.selectEntity(CourseClass)}>Courses</a>
                        </li>
                        <li>
                            <a href="#" onClick={this.selectEntity(PostClass)}>Posts</a>
                        </li>
                    </ul>
                </div>
                <div className="col-md-8">
                {crudTable}
                </div>
            </div>            
            );
    },
    selectEntity: function (entity) {
        //this.refs.crudTable.selectEntity(entity);
    }
});

React.renderComponent(<AdminPanel />, document.getElementById('app'));
