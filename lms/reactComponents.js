
// STUBS OF REACT COMPONENTS


var CrudTable = React.createClass({
    getInitialState: function() {
        return {className: "none"};
    },

    render: function () {

        return (
            <div>
            Render CRUD Table for entity: {this.state.className}
            </div>            );
    },
    selectEntity: function (ent) {
        this.setState({className: ent});
    }
});

var Form = React.createClass({
    render: function () {

        return (
            <form>
            Render Form
            </form>            );
    }
});

//var Dropdown = React.createClass({
//
//// render a dropdown for a field, option
//
// });
//
//var UsersPicker = React.createClass({
//
//// ...
//
// });
//
//var AvailabilityEditor = React.createClass({
//
//// ...
//
// });


