// Need to define following data Types:
// Type.STRING
// Type.ARRAY
// Type.OBJECT (i.e. JSON)
// Type.DATE
// Type.BOOLEAN
// Type.NUMBER

// An InputComponent is a React Component which defines a UI element used to edit a field as specified in an Entity Definition
// (for example a Dropdown or CheckboxList).  When not specified, default UI types are inferred from data types (i.e. a String is
// a TextBox, a boolean is a checkBox).
// We need the following custom UI elements:
// Dropdown
// CheckboxList
// UsersPicker
// AvailabilityEditor

var CourseClass = Entity.define('Course', {
    name: Type.STRING,
    description: Type.STRING,
    teachers: {type: Type.ARRAY, inputComponent: UsersPicker },
    students: {type: Type.ARRAY, inputComponent: UsersPicker }
});

var PostClass = Entity.define('Post', {
    title: Type.STRING,
    content: Type.STRING,
    author: Type.STRING //pointer to User Object
});

var SurveyClass = Entity.define('Course', {
    firstName: {label: "First Name", type: Type.STRING },
    lastName: {label: "Last Name", type: Type.STRING },
    dob: {label: "Date of Birth", type: Type.DATE },
    country: {label: "Country", type: Type.STRING, inputComponent: Dropdown, options: CountryList },
    languages: {label: "Languages", type: Type.ARRAY, inputComponent: CheckboxList, options: LanguageList },
    availability: {label: "Availability", type: Type.OBJECT, inputComponent: AvailabilityEditor }
});

// *** We Also need to display table for User Entity, which is a special built-in Parse Object
